from django.contrib.auth import login
from django.http.response import HttpResponseServerError
from django.shortcuts import redirect, render
from django.urls import reverse
from users.forms import CustomUserCreationForm


def dashboard(request):
    return render(request, "users/dashboard.html")

def register(request):
    if request.method == "GET":
        return render(
            request, "users/register.html",
            {"form": CustomUserCreationForm}
        )
    elif request.method == "POST":
        print(request.POST)
        form = CustomUserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect(reverse("configuration"))
        return HttpResponseServerError('<center><h1>Error, please retry <a href="{% url \'register\' %}">here</a></h1></center>')