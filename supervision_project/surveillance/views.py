from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.contrib import messages
from django.views import View 
from datetime import datetime
from io import StringIO
import matplotlib.pyplot as plt
import numpy as np
import yaml


class ViewIndex(LoginRequiredMixin, View):
    def get(self, request):

        return render(request, "surveillance.html", {
        })

class ViewGraphDebit(LoginRequiredMixin, View):
    def get(self, request, id_machine):

        yaml_file_surveillance = open("surveillance/host_data.yaml")
        parsed_yaml_file = yaml.load(yaml_file_surveillance, Loader=yaml.FullLoader)

        yaml_file_config = open("configuration/data.yaml")
        config_parsed_yaml_file = yaml.load(yaml_file_config, Loader=yaml.FullLoader)

        try:
            data_yaml = parsed_yaml_file["machines"][id_machine]
            list_time = []
            list_debit = []

            for index in data_yaml:
                time = int(data_yaml[index]["time"])
                list_time.append(datetime.utcfromtimestamp(time).strftime('%H:%M:%S'))
                list_debit.append(int((data_yaml[index]["debit"]/1000)))

            fig, ax2 = plt.subplots(figsize=(18, 8))

            N = len(list_time)
            ind = np.arange(N)  

            def format_date(x, pos=None):
                thisind = np.clip(int(x + 0.5), 0, N - 1)
                return list_time[thisind]

            ax2.plot(ind, list_debit, '-')
            
            ax2.xaxis.set_major_formatter(format_date)
            ax2.set_title("Graphe débit")
            threshold  = config_parsed_yaml_file["machines"][id_machine]["octet_trigger"]
            ax2.hlines(y=int(threshold), xmin=0, xmax=(N+1), color='r', linestyle=':')

            plt.xlabel("Temps")
            plt.ylabel("Débit (Kbit/s)")

            fig.autofmt_xdate()

            imgdata = StringIO()
            fig.savefig(imgdata, format='svg')
            imgdata.seek(0)

            data = imgdata.getvalue()
            

            return render(request, "surveillance.html", {
                'graph': data,
                'items': ["ram", "cpu"],
                'cur_item': 'debit'
            })

        except KeyError as e:
            messages.add_message(request, messages.ERROR, e)
            return render(request, 'error.html')
        

class ViewGraphRam(LoginRequiredMixin, View):
    def get(self, request, id_machine):

        yaml_file_surveillance = open("surveillance/host_data.yaml")
        parsed_yaml_file = yaml.load(yaml_file_surveillance, Loader=yaml.FullLoader)

        yaml_file_config = open("configuration/data.yaml")
        config_parsed_yaml_file = yaml.load(yaml_file_config, Loader=yaml.FullLoader)

        try:
            data_yaml = parsed_yaml_file["machines"][id_machine]
            list_time = []
            list_ram = []

            for index in data_yaml:
                time = int(data_yaml[index]["time"])
                list_time.append(datetime.utcfromtimestamp(time).strftime('%H:%M:%S'))
                # list_time.append(int(data_yaml[index]["time"]))
                list_ram.append(int(data_yaml[index]["ram"]))

            fig, ax2 = plt.subplots(figsize=(18, 8))

            plt.xlabel("Temps")
            plt.ylabel("RAM (%)")

            N = len(list_time)
            ind = np.arange(N)  

            def format_date(x, pos=None):
                thisind = np.clip(int(x + 0.5), 0, N - 1)
                return list_time[thisind]


            ax2.plot(ind, list_ram, 'o-')
            ax2.xaxis.set_major_formatter(format_date)
            ax2.set_title("Graphe RAM")
            threshold  = config_parsed_yaml_file["machines"][id_machine]["ram_trigger"]
            ax2.hlines(y=int(threshold), xmin=0, xmax=(N+1), color='r', linestyle=':')


            fig.autofmt_xdate()

            ax2.set_ylim(0,100)

            imgdata = StringIO()
            fig.savefig(imgdata, format='svg')
            imgdata.seek(0)

            data = imgdata.getvalue()

            return render(request, "surveillance.html", {
                'graph': data,
                'items': ["debit", "cpu"],
                'cur_item': 'ram'
            })

        except KeyError as e:
            messages.add_message(request, messages.ERROR, e)
            return render(request, 'error.html')


class ViewGraphCpu(LoginRequiredMixin, View):
    def get(self, request, id_machine):

        yaml_file_surveillance = open("surveillance/host_data.yaml")
        parsed_yaml_file = yaml.load(yaml_file_surveillance, Loader=yaml.FullLoader)

        yaml_file_config = open("configuration/data.yaml")
        config_parsed_yaml_file = yaml.load(yaml_file_config, Loader=yaml.FullLoader)

        try:
            data_yaml = parsed_yaml_file["machines"][id_machine]
            list_time = []
            list_cpu = []

            for index in data_yaml:
                time = int(data_yaml[index]["time"])
                list_time.append(datetime.utcfromtimestamp(time).strftime('%H:%M:%S'))
                # list_time.append(int(data_yaml[index]["time"]))
                list_cpu.append(int(data_yaml[index]["cpu"]))

            fig, ax2 = plt.subplots(figsize=(18, 8))

            plt.xlabel("Temps")
            plt.ylabel("CPU (%)")

            N = len(list_time)
            ind = np.arange(N)  

            def format_date(x, pos=None):
                thisind = np.clip(int(x + 0.5), 0, N - 1)
                return list_time[thisind]


            ax2.plot(ind, list_cpu, 'o-')
            # Use automatic FuncFormatter creation
            ax2.xaxis.set_major_formatter(format_date)
            ax2.set_title("Graphe CPU")
            threshold  = config_parsed_yaml_file["machines"][id_machine]["cpu_trigger"]
            ax2.hlines(y=int(threshold), xmin=0, xmax=(N+1), color='r', linestyle=':')
            fig.autofmt_xdate()

            ax2.set_ylim(0,100)

            imgdata = StringIO()
            fig.savefig(imgdata, format='svg')
            imgdata.seek(0)

            data = imgdata.getvalue()

            return render(request, "surveillance.html", {
                'graph': data,
                'items': ["debit", "ram"],
                'cur_item': 'cpu'
            })

        except KeyError as e:
            messages.add_message(request, messages.ERROR, e)
            return render(request, 'error.html')

        