from pyasn1.compat import octets
from pysnmp.hlapi import *
from pysnmp.hlapi import varbinds
from pysnmp.error import PySnmpError
import time
import datetime
import yaml
import subprocess


# cwd = os.getcwd()  # Get the current working directory (cwd)
# files = os.listdir(cwd)  # Get all the files in that directory
# print("Files in %r: %s" % (cwd, files))

def collect():
    yaml_file = open("configuration/data.yaml")
    parsed_yaml_file = yaml.load(yaml_file, Loader=yaml.FullLoader)

    machines = parsed_yaml_file["machines"]

    for machine in machines:
        if machines[machine]["monitoring"] == True:

            with open('surveillance/host_data.yaml','r') as yamlfile:
                cur_yaml = yaml.safe_load(yamlfile) # Note the safe_load

                current_time = int(time.time()+7200)

                try:
                    index = len(cur_yaml["machines"][machine])
                except:
                    index = 1

                cpu = 0
                ram = 0
                octets = 0
                debit = 0

                if machines[machine]["cpu"]:
                    try:
                        g = nextCmd(SnmpEngine()
                                    , CommunityData(machines[machine]["communaute"], mpModel=1) # 
                                    , UdpTransportTarget((machines[machine]["ip"], 161)) # demo.snmplabs.com #
                                    , ContextData()
                                    , ObjectType(ObjectIdentity("1.3.6.1.2.1.25.3.3.1.2"))) # octect IN 1.3.6.1.2.1.2.2.1.10   CPU :1.3.6.1.2.1.25.3.3.1.2  RAM : 1.3.6.1.4.1.2021.4.6 en kb

                        errorIndication, errorStatus, errorIndex, varBinds = next(g)
                    except PySnmpError as error:
                        errorIndication = error

                    if errorIndication:
                        log_managment(machine, errorIndication, machines[machine]["ip"])
                    else:
                        try:
                            for varBind in varBinds:
                                cpu = int(varBind.__getitem__(1))

                            cpu_trigger = machines[machine]["cpu_trigger"]
                            if cpu_trigger != '0':
                                    if int(cpu) > int(cpu_trigger):
                                        errorIndication = f"ALERTE : CPU ! (cpu : {round(cpu, 2)}%)"
                                        log_managment(machine, errorIndication, machines[machine]["ip"])
                        except:
                            errorIndication = "Erreur serveur : Connexion avec l'hôte impossible"
                            log_managment(machine, errorIndication, machines[machine]["ip"])

                if machines[machine]["ram"]:
                    try:
                        g = nextCmd(SnmpEngine()
                                    , CommunityData(machines[machine]["communaute"], mpModel=1) # 
                                    , UdpTransportTarget((machines[machine]["ip"], 161)) # demo.snmplabs.com #
                                    , ContextData()
                                    , ObjectType(ObjectIdentity("1.3.6.1.4.1.2021.4.5"))) # octect IN 1.3.6.1.2.1.2.2.1.10   CPU :1.3.6.1.2.1.25.3.3.1.2  RAM : 1.3.6.1.4.1.2021.4.6 en kb

                        errorIndication, errorStatus, errorIndex, varBinds = next(g)
                    
                    except PySnmpError as error:
                        errorIndication = error

                    if errorIndication:
                        log_managment(machine, errorIndication, machines[machine]["ip"])
                    else:
                        for varBind in varBinds:
                            ramTotal = int(varBind.__getitem__(1))

                        try:
                            g = nextCmd(SnmpEngine()
                                        , CommunityData(machines[machine]["communaute"], mpModel=1) # 
                                        , UdpTransportTarget((machines[machine]["ip"], 161)) # demo.snmplabs.com #
                                        , ContextData()
                                        , ObjectType(ObjectIdentity("1.3.6.1.4.1.2021.4.6"))) # octect IN 1.3.6.1.2.1.2.2.1.10   CPU :1.3.6.1.2.1.25.3.3.1.2  RAM : 1.3.6.1.4.1.2021.4.6 en kb

                            errorIndication, errorStatus, errorIndex, varBinds = next(g)
                        
                        except PySnmpError as error:
                            errorIndication = error

                        if errorIndication:
                            log_managment(machine, errorIndication, machines[machine]["ip"])
                        else:
                            try:
                                for varBind in varBinds:
                                    ramUnused = int(varBind.__getitem__(1))

                                ramUsed = ramTotal - ramUnused
                                ram = (ramUsed/ramTotal)*100

                                ram_trigger = machines[machine]["ram_trigger"]
                                if ram_trigger != '0':
                                    if int(ram) > int(ram_trigger):
                                        errorIndication = f"ALERTE : RAM ! (ram : {round(ram, 2)}%)"
                                        log_managment(machine, errorIndication, machines[machine]["ip"])
                            except:
                                errorIndication = "Erreur serveur : Connexion avec l'hôte impossible"
                                log_managment(machine, errorIndication, machines[machine]["ip"])


                if machines[machine]["debit"]:
                    try:
                        g = nextCmd(SnmpEngine()
                                    , CommunityData(machines[machine]["communaute"], mpModel=1) # 
                                    , UdpTransportTarget((machines[machine]["ip"], 161)) # demo.snmplabs.com #
                                    , ContextData()
                                    , ObjectType(ObjectIdentity("1.3.6.1.2.1.2.2.1.10"))) # octect IN 1.3.6.1.2.1.2.2.1.10   CPU :1.3.6.1.2.1.25.3.3.1.2  RAM : 1.3.6.1.4.1.2021.4.6 en kb

                        errorIndication, errorStatus, errorIndex, varBinds = next(g)
                    except PySnmpError as error:
                        errorIndication = error

                    if errorIndication:
                        log_managment(machine, errorIndication, machines[machine]["ip"])
                    else:
                        try:
                            for varBind in varBinds:
                                octets = int(varBind.__getitem__(1))

                            if machine in cur_yaml["machines"]:
                                last_octets = cur_yaml["machines"][machine][index]["octet"]
                                temps_valeurs = current_time - (cur_yaml["machines"][machine][index]["time"])

                                if octets < last_octets:
                                    debit = (((4294967295-last_octets)+octets)*8)/temps_valeurs
                                else: 
                                    debit = ((octets-last_octets)*8)/temps_valeurs

                                octet_trigger = machines[machine]["octet_trigger"]
                                if octet_trigger != '0':
                                    if int(debit) > (int(octet_trigger)*1000):
                                        errorIndication = f"ALERTE : Débit ! (débit : {round(debit/1000, 2)} KB/s)"
                                        log_managment(machine, errorIndication, machines[machine]["ip"])
                    
                                index += 1
                            else:
                                cur_yaml["machines"][machine] = {}
                        except:
                            errorIndication = "Erreur serveur : Connexion avec l'hôte impossible"
                            log_managment(machine, errorIndication, machines[machine]["ip"])
                
                
                
                if (debit < 150000) and (cpu < 150) and (ram < 150) :
                    new_yaml_data_dict = {
                        "cpu": cpu,
                        "ram": ram,
                        "octet": octets,
                        "debit": debit,
                        "time": current_time
                    }
                    cur_yaml["machines"][machine][index] = new_yaml_data_dict
                else:
                    new_yaml_data_dict = {
                        "cpu": cpu,
                        "ram": ram,
                        "octet": 0,
                        "debit": debit,
                        "time": current_time
                    }
                    print('Valeur incohérente : débit {}Kbit/s | cpu {}% | ram {}%'.format(debit,cpu,ram))
                

                if cur_yaml:
                    with open('surveillance/host_data.yaml','w') as yamlfile:
                        yaml.safe_dump(cur_yaml, yamlfile) # Also note the safe_dump


def log_managment(machine, errorIndication, ip):

    errorIndication = str(errorIndication)
    
    if errorIndication.startswith('No SNMP response received before timeout'):
        errorIndication = "Aucune réponse SNMP reçu avant le timeout : Vérifier votre communauté."
    elif errorIndication.startswith('Bad IPv4/UDP'):
        errorIndication = f"Mauvaise adresse IPv4/UDP : {ip}."        

    current_time = datetime.datetime.now()

    with open('log/log_data.yaml','r') as yamlfile:
        cur_yaml = yaml.safe_load(yamlfile) # Note the safe_load
        
        try:
            index = len(cur_yaml)
            index = index + 1
        except:
            index = 1

        new_yaml_data_dict = {
            "machine": f"{machine}",
            "time": f"{current_time.strftime('%Y-%m-%d %H:%M:%S')}",
            "message": f"{errorIndication}",
        }

        cur_yaml[index] = new_yaml_data_dict

        if cur_yaml:
            with open('log/log_data.yaml','w') as yamlfile:
                yaml.safe_dump(cur_yaml, yamlfile) # Also note the safe_dump