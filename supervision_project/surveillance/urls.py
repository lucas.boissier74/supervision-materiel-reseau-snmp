from django.urls import path
from surveillance.views import ViewIndex, ViewGraphDebit, ViewGraphRam, ViewGraphCpu

urlpatterns = [
    path('', ViewIndex.as_view(), name="surveillance"),
    path('<str:id_machine>/debit', ViewGraphDebit.as_view(), name='graph-debit'),
    path('<str:id_machine>/ram', ViewGraphRam.as_view(), name='graph-ram'),
    path('<str:id_machine>/cpu', ViewGraphCpu.as_view(), name='graph-cpu'),
]