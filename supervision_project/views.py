from django.shortcuts import redirect
from django.views.generic import TemplateView

class ViewIndex(TemplateView):
    def get(self, request):

        # recup data depuis yaml ?

        return redirect('/configuration/')