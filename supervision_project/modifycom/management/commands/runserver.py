from django.contrib.staticfiles.management.commands.runserver import Command as StaticfilesRunserverCommand
from apscheduler.schedulers.background import BackgroundScheduler
from surveillance.main import collect

def start():
    scheduler = BackgroundScheduler()  
    print("collect")
    scheduler.add_job(collect, 'interval', seconds=5 )
    scheduler.start()

class Command(StaticfilesRunserverCommand):
    def handle(self, *args, **options):
        self.stdout.write("------ admin command modifed -------")
        start()
        super(Command, self).handle(*args, **options)
        