from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect
from collections import OrderedDict
from django.views import View 
import yaml
import os

# Create your views here.

class ViewIndex(LoginRequiredMixin, View):
    def get(self, request):
        yaml_file = open(r"log/log_data.yaml")
        parsed_yaml_file = yaml.load(yaml_file, Loader=yaml.FullLoader)

        parsed_yaml_file = OrderedDict(reversed(list(parsed_yaml_file.items())))

        return render(request, "logs.html", {
            'datas': parsed_yaml_file,
        })

class ViewDelete(LoginRequiredMixin, View):
    def get(self, request):
        file = 'log/log_data.yaml'
        if os.path.exists(file):
            os.remove(file)

        with open(file, 'w') as f:
            f.write('{}')

        return redirect("/log/")

