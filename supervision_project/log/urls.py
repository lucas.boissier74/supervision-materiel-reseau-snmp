from django.urls import path
from log.views import ViewIndex, ViewDelete

urlpatterns = [
    path('', ViewIndex.as_view(), name="log"),
    path('delete/', ViewDelete.as_view(), name="delete-log"),
]