from django import forms

class CreateForm(forms.Form):
    name = forms.CharField()
    ip = forms.CharField()
    description = forms.CharField()
    octet_trigger = forms.CharField()
    ram_trigger = forms.CharField()
    cpu_trigger = forms.CharField()
    communaute = forms.CharField()
    monitoring = forms.BooleanField(
        required=False,
        initial=False)
    debit = forms.BooleanField(
        required=False,
        initial=False)
    ram = forms.BooleanField(
        required=False,
        initial=False)
    cpu = forms.BooleanField(
        required=False,
        initial=False)

class ModifyForm(forms.Form):
    name = forms.CharField()
    ip = forms.CharField()
    description = forms.CharField()
    octet_trigger = forms.CharField()
    ram_trigger = forms.CharField()
    cpu_trigger = forms.CharField()
    communaute = forms.CharField()
    monitoring = forms.BooleanField(
        required=False,
        initial=False)
    debit = forms.BooleanField(
        required=False,
        initial=False)
    ram = forms.BooleanField(
        required=False,
        initial=False)
    cpu = forms.BooleanField(
        required=False,
        initial=False)