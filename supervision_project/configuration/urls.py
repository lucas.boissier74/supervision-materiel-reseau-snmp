from django.urls import path
from configuration.views import ViewIndex, ViewCreate, ViewDelete, ViewModify, ViewSwitch

urlpatterns = [
    path('', ViewIndex.as_view(), name="configuration"),
    path('create/', ViewCreate.as_view(), name='create'),
    path('update/', ViewModify.as_view(), name='update'),
    path('<str:id_machine>/', ViewDelete.as_view(), name="delete"),
    path('modify/<str:id_machine>/', ViewModify.as_view(), name='modify'),
    path('<str:id_machine>/<value>/', ViewSwitch.as_view(), name='monitoring'),
]