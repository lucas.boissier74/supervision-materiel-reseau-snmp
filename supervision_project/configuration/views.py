from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect
from .forms import CreateForm, ModifyForm
from collections import OrderedDict
from django.views import View 
import yaml


class ViewIndex(LoginRequiredMixin, View):
    # @login_required(login_url='/accounts/login/')
    def get(self, request):
        yaml_file = open(r"configuration/data.yaml")
        parsed_yaml_file = yaml.load(yaml_file, Loader=yaml.FullLoader)
        
        yaml_file_log = open(r"log/log_data.yaml")
        parsed_yaml_file_log = yaml.load(yaml_file_log, Loader=yaml.FullLoader)

        parsed_yaml_file_log = OrderedDict(reversed(list(parsed_yaml_file_log.items())))

        return render(request, "configuration.html", {
            'datas': parsed_yaml_file['machines'],
            'datas_log': parsed_yaml_file_log
        })


class ViewCreate(LoginRequiredMixin, View):
    def get(self, request):

        return render(request, "create.html")
    def post(self, request):
        form = CreateForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            ip = form.cleaned_data['ip']
            monitoring = form.cleaned_data['monitoring']
            octet_trigger = form.cleaned_data['octet_trigger']
            ram_trigger = form.cleaned_data['ram_trigger']
            cpu_trigger = form.cleaned_data['cpu_trigger']
            description = form.cleaned_data['description']
            communaute = form.cleaned_data['communaute']
            cpu = form.cleaned_data['cpu']
            ram = form.cleaned_data['ram']
            debit = form.cleaned_data['debit']

            new_yaml_data_dict = {       
                'ip': ip,
                'monitoring': monitoring, 
                'octet_trigger': octet_trigger, 
                'ram_trigger': ram_trigger, 
                'cpu_trigger': cpu_trigger, 
                'description': description,
                'communaute': communaute,
                'cpu': cpu,
                'ram': ram,
                'debit': debit,
            }

            with open('configuration/data.yaml','r') as yamlfile:
                cur_yaml = yaml.safe_load(yamlfile) # Note the safe_load
                cur_yaml['machines'][name] = new_yaml_data_dict

            if cur_yaml:
                with open('configuration/data.yaml','w') as yamlfile:
                    yaml.safe_dump(cur_yaml, yamlfile) # Also note the safe_dump
        
        return redirect("/configuration/")


class ViewDelete(LoginRequiredMixin, View):
    def post(self, request, id_machine):
        
        with open('configuration/data.yaml','r') as yamlfile:
            cur_yaml = yaml.safe_load(yamlfile) # Note the safe_load
            del cur_yaml['machines'][id_machine]           
        if cur_yaml:
            with open('configuration/data.yaml','w') as yamlfile:
                yaml.safe_dump(cur_yaml, yamlfile) # Also note the safe_dump

        return redirect("/configuration/")
    
    
class ViewSwitch(LoginRequiredMixin, View):
    def post(self, request, id_machine, value:bool):
        if value == "false":
            monitoring = False
        else:
            monitoring = True
        with open('configuration/data.yaml','r') as yamlfile:
            cur_yaml = yaml.safe_load(yamlfile) # Note the safe_load
            cur_yaml['machines'][id_machine]['monitoring'] = monitoring        
        if cur_yaml:
            with open('configuration/data.yaml','w') as yamlfile:
                yaml.safe_dump(cur_yaml, yamlfile) # Also note the safe_dump

        return redirect("/configuration/")    


class ViewModify(LoginRequiredMixin, View):
    def get(self, request, id_machine):

        with open('configuration/data.yaml','r') as yamlfile:
            cur_yaml = yaml.safe_load(yamlfile) # Note the safe_load
            description = cur_yaml['machines'][id_machine]['description']
            ip = cur_yaml['machines'][id_machine]['ip']
            monitoring = cur_yaml['machines'][id_machine]['monitoring']
            communaute = cur_yaml['machines'][id_machine]['communaute']
            cpu = cur_yaml['machines'][id_machine]['cpu']
            rams = cur_yaml['machines'][id_machine]['ram']
            debit = cur_yaml['machines'][id_machine]['debit']
            octet_trigger = cur_yaml['machines'][id_machine]['octet_trigger']
            ram_trigger = cur_yaml['machines'][id_machine]['ram_trigger']
            cpu_trigger = cur_yaml['machines'][id_machine]['cpu_trigger']
            
            for k in cur_yaml['machines'].keys():
                if k == id_machine:
                    name = k

        return render(request, "modify.html", {
            'name': name,
            'description': description,
            'monitoring': monitoring,
            'ip': ip,
            'octet_trigger': octet_trigger,
            'ram_trigger': ram_trigger, 
            'cpu_trigger': cpu_trigger, 
            'communaute': communaute,
            'cpu': cpu,
            'ram': rams,
            'debit': debit,
        })

    def post(self, request):
        form = ModifyForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            ip = form.cleaned_data['ip']
            monitoring = form.cleaned_data['monitoring']
            octet_trigger = form.cleaned_data['octet_trigger']
            ram_trigger = form.cleaned_data['ram_trigger']
            cpu_trigger = form.cleaned_data['cpu_trigger']
            description = form.cleaned_data['description']
            communaute = form.cleaned_data['communaute']
            cpu = form.cleaned_data['cpu']
            rams = form.cleaned_data['ram']
            debit = form.cleaned_data['debit']
            
            new_yaml_data_dict = {       
                'ip': ip,
                'monitoring': monitoring, 
                'octet_trigger': octet_trigger,
                'ram_trigger': ram_trigger, 
                'cpu_trigger': cpu_trigger, 
                'description': description,
                'communaute': communaute,
                'cpu': cpu,
                'ram': rams,
                'debit': debit,
            }

            with open('configuration/data.yaml','r') as yamlfile:
                cur_yaml = yaml.safe_load(yamlfile) # Note the safe_load
                cur_yaml['machines'][name] = new_yaml_data_dict

            if cur_yaml:
                with open('configuration/data.yaml','w') as yamlfile:
                    yaml.safe_dump(cur_yaml, yamlfile) # Also note the safe_dump
        
        return redirect("/configuration/")
